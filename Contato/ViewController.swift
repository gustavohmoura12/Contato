//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let email: String
    let endereco: String
    let numero: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContato:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContato.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContato[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self

        listaDeContato.append((Contato(nome: "Contato 1", email: "contato1@email.com", endereco: "Rua Cristal, 12", numero: "31-99543-7744")))
        listaDeContato.append((Contato(nome: "Contato 2", email: "contato2@email.com", endereco: "Rua Cristal, 13", numero: "31-99542-7744")))
        listaDeContato.append((Contato(nome: "Contato 3", email: "contato3@email.com", endereco: "Rua Cristal, 14", numero: "31-99541-7744")))
        
    }


}


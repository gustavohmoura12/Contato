//
//  MyCell.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var nome: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var numero: UILabel!
    
    @IBOutlet weak var endereco: UILabel!
}
